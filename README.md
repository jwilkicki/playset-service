# playset-service
Playset microservice for Fiasco

![Go](https://github.com/jwilkicki/playset-service/workflows/Go/badge.svg)

This repository is for the microservice that manages available playsets for Fiasco games.
