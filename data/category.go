package data

import (
	"errors"
	"fmt"
)

// Category is a collection of related elements
type Category struct {
	Name     string
	Elements []string
}

// NewCategory creates a new category
func NewCategory(name string, elements []string) (Category, error) {
	if name == "" {
		return Category{}, errors.New("Category name cannot be empty")
	}

	if len(elements) != 6 {
		return Category{}, errors.New("Category must have six elements")
	}

	for i, element := range elements {
		if element == "" {
			return Category{}, fmt.Errorf("Element %d cannot be empty", i)
		}
	}

	return Category{Name: name, Elements: elements}, nil
}

func (category Category) validate() []error {
	errorList := make([]error, 0)

	if category.Name == "" {
		errorList = append(errorList, fmt.Errorf("category must have a name"))
	}

	if len(category.Elements) != 6 {
		errorList = append(errorList, fmt.Errorf("%s must have six elements", category.Name))
	} else {
		for i, element := range category.Elements {
			if element == "" {
				errorList = append(errorList, fmt.Errorf("Element %d cannot be empty", i))
			}
		}
	}

	return errorList
}
