package data

import (
	"errors"
	"fmt"
)

// Playset is a collection of elements that defines a theme for a Fiasco game
type Playset struct {
	Name          string
	Description   string
	Summary       string
	Relationships []Category
	Locations     []Category
	Needs         []Category
	Objects       []Category
	Tilt          []Category
}

// NewPlayset creates a new Playset
func NewPlayset(name, summary, description string) (Playset, error) {
	if name == "" {
		return Playset{}, errors.New("name cannot be empty")
	}

	return Playset{Name: name, Summary: summary, Description: description}, nil
}

// Validate checks that a Playset has everything it needs to be playable.
// If the returned slice has no errors, then the Playset is valid.
func (playset *Playset) Validate() []error {
	errorList := make([]error, 0)

	if playset.Name == "" {
		errorList = append(errorList, errors.New("Playset must have a name"))
	}

	errorList = append(errorList, validateCategories("Relationships", playset.Relationships)...)

	errorList = append(errorList, validateCategories("Locations", playset.Locations)...)

	errorList = append(errorList, validateCategories("Needs", playset.Needs)...)

	errorList = append(errorList, validateCategories("Objects", playset.Objects)...)

	if len(playset.Tilt) > 0 {
		errorList = append(errorList, validateCategories("Tilt", playset.Tilt)...)
	}

	return errorList
}

func validateCategories(name string, categories []Category) []error {
	errorList := make([]error, 0)

	if len(categories) != 6 {
		errorList = append(errorList, fmt.Errorf("%s must have six categories", name))
	} else {
		for _, category := range categories {
			categoryErrors := category.validate()
			if len(categoryErrors) > 0 {
				errorList = append(errorList, fmt.Errorf("%s Category %s has %d errors: %v", name, category.Name, len(categoryErrors), categoryErrors))
			}
		}
	}

	return errorList
}
