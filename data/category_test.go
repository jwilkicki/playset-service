package data

import "testing"

func TestNewCategory(t *testing.T) {
	tests := []struct {
		name        string
		elements    []string
		expectError bool
	}{
		{
			name:        "foo",
			elements:    []string{"one", "two", "three", "four", "five", "six"},
			expectError: false,
		},
		{
			name:        "",
			elements:    []string{"one", "two", "three", "four", "five", "six"},
			expectError: true,
		},
		{
			name:        "foo",
			elements:    []string{"one"},
			expectError: true,
		},
	}

	for _, test := range tests {
		category, err := NewCategory(test.name, test.elements)

		if test.expectError && err == nil {
			t.Error("Expected error and didn't get one")
		} else if !test.expectError && err != nil {
			t.Errorf("Did not expect error and got %v", err)
		} else if err == nil {
			if category.Name != test.name {
				t.Errorf("Expected %s, got %s", test.name, category.Name)
			}

			if len(category.Elements) != len(test.elements) {
				t.Errorf("Length of elements didn't match; expected %d, got %d", len(test.elements), len(category.Elements))
			}

			for i := range test.elements {
				if category.Elements[i] != test.elements[i] {
					t.Errorf("Expected element %d to be %v; got %v", i, test.elements[i], category.Elements[i])
				}
			}
		}
	}
}

func TestValidate(t *testing.T) {
	tests := []struct {
		underTest   Category
		expectError int
	}{
		{
			underTest: Category{
				Name: "pass",
				Elements: []string{
					"one",
					"two",
					"three",
					"four",
					"five",
					"six",
				},
			},
			expectError: 0,
		},
		{
			underTest: Category{
				Name: "",
				Elements: []string{
					"",
					"",
					"",
					"",
					"",
					"",
				},
			},
			expectError: 7,
		},
		{
			underTest: Category{
				Name: "toosmall",
				Elements: []string{
					"one",
				},
			},
			expectError: 1,
		},
	}

	for _, test := range tests {
		errors := test.underTest.validate()
		if len(errors) != test.expectError {
			t.Errorf("Expected %d errors but got %v", test.expectError, errors)
		}
	}
}
