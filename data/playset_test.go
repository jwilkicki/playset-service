package data

import "testing"

func TestNewPlayset(t *testing.T) {
	tests := []struct {
		name        string
		summary     string
		description string
		expectError bool
	}{
		{
			name:        "foo",
			summary:     "foo summary",
			description: "foo description",
			expectError: false,
		},
		{
			name:        "",
			summary:     "bar summary",
			description: "bar description",
			expectError: true,
		},
	}

	for _, test := range tests {
		playset, err := NewPlayset(test.name, test.summary, test.description)

		if test.expectError && err == nil {
			t.Error("Expected error and didn't get one")
		} else if !test.expectError && err != nil {
			t.Errorf("Did not expect error and got %v", err)
		} else if err == nil {
			if playset.Name != test.name {
				t.Errorf("Expected %s, got %s", test.name, playset.Name)
			}

			if playset.Description != test.description {
				t.Errorf("Expected %s, got %s", test.description, playset.Description)
			}

			if playset.Summary != test.summary {
				t.Errorf("Expected %s, got %s", test.summary, playset.Summary)
			}
		}
	}

}

func TestPlaysetValidate(t *testing.T) {
	tests := []struct {
		underTest      Playset
		expectedErrors int
	}{
		{
			underTest: Playset{
				Name:        "pass",
				Description: "pass",
				Summary:     "pass",
				Relationships: []Category{
					{
						Name:     "One",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Two",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Three",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Four",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Five",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Six",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					}},
				Locations: []Category{
					{
						Name:     "One",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Two",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Three",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Four",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Five",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Six",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					}},
				Needs: []Category{
					{
						Name:     "One",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Two",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Three",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Four",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Five",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Six",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					}},
				Objects: []Category{
					{
						Name:     "One",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Two",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Three",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Four",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Five",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Six",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					}},
				Tilt: []Category{
					{
						Name:     "One",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Two",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Three",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Four",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Five",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Six",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					}},
			},
			expectedErrors: 0,
		},
		{
			underTest: Playset{
				Name:        "",
				Description: "fail",
				Summary:     "fail",
				Relationships: []Category{
					{
						Name:     "One",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Two",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Three",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Four",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Five",
						Elements: []string{"one", "two", "three", "four", "five", "six"},
					},
					{
						Name:     "Six",
						Elements: []string{"one", "two", "three", "four", "five"},
					}},
			},
			expectedErrors: 5,
		},
	}

	for _, test := range tests {
		errors := test.underTest.Validate()
		if len(errors) != test.expectedErrors {
			t.Errorf("Expected %d errors but got %v", test.expectedErrors, errors)
		}
	}
}
