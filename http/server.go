package http

import(
  "fmt"
  "net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
 )
 
 func Run(port int) {
    r := chi.NewRouter()
    r.Use(middleware.Logger)
    r.Get("/", func(w http.ResponseWriter, r *http.Request){
	    w.Write([]byte("Init"))
    })
    http.ListenAndServe(fmt.Sprintf(":%d",port),r)  
 }
